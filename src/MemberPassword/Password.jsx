import React, { useState } from "react";

import { TextField, Button } from "@material-ui/core";
import { useParams } from "react-router";
import axios from "axios";

import { API } from "../../src/Backend";
import Loader from "../../src/loader/Loader";
import { Helmet } from "react-helmet";

const Password = () => {
  // param extraction
  let { memberId } = useParams("memberId");

  // useState hooks
  const [Values, setValues] = useState({
    password: "",
    confirmPassword: "",
    success: false,
  });

  const [Loading, setLoading] = useState(false);

  // destructuring
  const { password, confirmPassword, success } = Values;

  // changeHandler
  const changeHandler = (name) => (event) => {
    let value = event.target.value;
    setValues({ ...Values, [name]: value });
  };

  // submitHandler
  const submitHandler = (e) => {
    e.preventDefault();

    if (!password || !confirmPassword) {
      alert("Please Insert Both Fields First!");
      return "";
    }

    setLoading(true);

    axios
      .put(`${API}api/admin/member/${memberId}/update/password`, Values)
      .then((data) => {
        if (data.error) {
          alert(data.message);
        }
        console.log("DATA", data);
        alert(data.data.message);
        setValues({
          password: "",
          confirmPassword: "",
          success: true,
        });
        setLoading(false);
      })
      .catch((err) => {
        console.log("ERROR", err);
        setValues({
          password: "",
          confirmPassword: "",
        });
        setLoading(false);
      });
  };

  console.log("VALUES", Values);
  console.log("MEMBER ID", memberId);

  if (Loading) {
    return <Loader />;
  }

  return (
    <>
      <Helmet>
        <title>Set Password</title>
      </Helmet>
      <div
        style={{
          height: "100vh",
          width: "100%",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        {success ? (
          <>
            <h3 style={{ textAlign: "center" }}>
              Thanks! You Can Now Login With This Password
            </h3>
            <br />
          </>
        ) : (
          <form
            style={{ width: "30%", textAlign: "center" }}
            onSubmit={submitHandler}
          >
            <h3>Set Password Here</h3>
            <TextField
              type="password"
              style={{ width: "100%" }}
              id="standard-basic"
              label="Password"
              defaultValue={password}
              onChange={changeHandler("password")}
            />
            <br />
            <br />

            <TextField
              type="password"
              style={{ width: "100%" }}
              id="standard-basic"
              label="Confirm Password"
              defaultValue={confirmPassword}
              onChange={changeHandler("confirmPassword")}
            />
            <br />
            <br />
            <br />

            <Button
              style={{ width: "100%" }}
              type="submit"
              variant="contained"
              color="primary"
            >
              Set Password
            </Button>
          </form>
        )}
      </div>
    </>
  );
};

export default Password;
