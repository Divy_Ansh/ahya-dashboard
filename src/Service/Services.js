import React, { useEffect, useRef, useState } from "react";
import { useForm } from "react-hook-form";
import Modalcomp from "../Modal/Modal";
import axios from "axios";
import Tables from "../Tables/ServicesTable";
import NavBar from "../Navbar/Navbar";
import Modal from "react-modal";
import { FormControlLabel, TextField } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import AddRoundedIcon from "@material-ui/icons/AddRounded";
import Select from "@material-ui/core/Select";
import { MenuItem, InputLabel, FormHelperText } from "@material-ui/core";
import ChipInput from "material-ui-chip-input";

import { API } from "../Backend";
import Loader from "../loader/Loader";
import { CompareArrowsOutlined, Edit } from "@material-ui/icons";
import { fileUpload } from "../uploads/UploadFile";

function Services() {
  let imageRef = useRef(null);

  //? useState hooks
  const [edit, setedit] = useState();
  const [Category, setCategory] = useState([]);
  const [ImagesUploading, setImagesUploading] = useState(false);
  const [CoverUploading, setCoverUploading] = useState(false);

  const [refresh, setrefresh] = useState(1);
  const [Loading, setLoading] = useState(true);
  const [Errors, setErrors] = useState({
    name: false,
    keywords: false,
    amount: false,
    duration: false,
    status: false,
    partner: false,
    imageUrl: false,
    galleryImages: false,
    cover: false,
  });
  const [modalIsOpen, setmodalIsOpen] = useState(false);

  function openModal() {
    setmodalIsOpen(true);
  }
  function closeModal() {
    setmodalIsOpen(false);
  }
  const customStyles = {
    content: {
      top: "50%",
      left: "50%",
      right: "auto",
      bottom: "auto",
      width: "25%",
      marginRight: "-50%",
      transform: "translate(-50%, -50%)",
    },
  };
  const [EditFormData, setEditFormData] = React.useState({
    name: "",
    imageUrl: "",
    // keywords: [],
    status: "true",
    category: "",
    partner: "",
    amount: "",
    duration: "",
    galleryImages: [],
  });

  const [Values, setValues] = useState({
    name: "",
    imageUrl: "",
    // keywords: [],
    status: "true",
    category: "",
    partner: "",
    amount: "",
    duration: "",
    galleryImages: [],
  });

  const {
    name,
    // keywords,
    amount,
    duration,
    status,
    category,
    partner,
    imageUrl,
    galleryImages,
  } = Values;

  const [Chips, setChips] = useState([]);
  const [EditChips, setEditChips] = useState("");
  const [Partners, setPartners] = useState([]);
  const [Categories, setCategories] = useState([]);

  // useEffect hooks
  useEffect(() => {
    getServices();
    getPartners();
    getCategories();
  }, [refresh]);

  function getServices() {
    axios
      .get(`${API}api/admin/all/services`)
      .then((data) => {
        setCategory(data.data);
        setValues({ ...Values, formData: new FormData() });
        setLoading(false);
      })
      .catch((err) => {
        console.log(err);
        setLoading(false);
      });
  }

  const getPartners = () => {
    axios
      .get(`${API}api/admin/service/partners`)
      .then((data) => {
        console.log(data);
        setPartners(data.data);
        setValues({ ...Values, formData: new FormData() });
      })

      .catch((err) => console.log(err));
  };

  const getCategories = () => {
    axios
      .get(`${API}api/admin/all/categories`)
      .then((data) => {
        console.log("CATEGORIES", data);
        setCategories(data.data);
        setValues({ ...Values, formData: new FormData() });
      })
      .catch((err) => console.log(err));
  };

  const Validate = () => {
    let value = true;

    let errors = {
      name: false,
      // keywords: false,
      amount: false,
      duration: false,
      status: false,
      partner: false,
      imageUrl: false,
      galleryImages: false,
      category: false,
    };

    if (imageUrl == "") {
      value = false;
      errors.imageUrl = "Please Select a Image First!";
    }

    if (name == "") {
      value = false;
      errors.name = "Please Enter Name First!";
    }

    if (amount == "") {
      value = false;
      errors.amount = "Please Enter Amount First!";
    }

    if (duration == "") {
      value = false;
      errors.duration = "Please Enter Duration First!";
    }

    if (partner == "") {
      value = false;
      errors.partner = "Please Select a Partner First!";
    }

    if (category == "") {
      value = false;
      errors.category = "Please Select Category First!";
    }

    // if (keywords == []) {
    //   value = false;
    //   errors.keywords = "Please Enter Keywords First!";
    // }

    if (galleryImages == []) {
      value = false;
      errors.galleryImages = "Please Enter Gallery Images First!";
    }

    let { current } = imageRef;

    if (current && current.naturalWidth && current.naturalWidth != 1680) {
      errors.imageUrl = "Please Select a cover with a width of 1680px!";
      value = false;
    }

    //? validation for image height
    if (current && current.naturalHeight && current.naturalHeight != 1050) {
      errors.imageUrl = "Please Select a cover with a Height of 1050px!";
      value = false;
    }

    setErrors({ ...errors });
    return value;
  };

  const submitHandler = (e) => {
    e.preventDefault();
    console.log(Values);

    if (Validate()) {
      setValues({ ...Values, keywords: [...Chips] });
      setLoading(true);

      axios
        .post(`${API}api/admin/create/service`, Values)
        .then((data) => {
          if (data.error) {
            alert(data.message);
          }
          setValues({
            name: "",
            imageUrl: "",
            keywords: [],
            status: "true",
            category: "",
            partner: "",
            amount: "",
            duration: "",
            imageUrl: "",
            galleryImage: "",
          });
          setrefresh((data) => data + 1);
          setLoading(false);
        })
        .catch((err) => {
          console.log(err);
          setValues({
            name: "",
            imageUrl: "",
            keywords: [],
            status: "true",
            category: "",
            partner: "",
            amount: "",
            duration: "",
            galleryImage: "",
          });
          alert(err);
          setLoading(false);
        });
    }
  };

  //? editChangeHandler
  const editChangeHandler = (name) => async (event) => {
    if (name === "imageUrl") {
      setCoverUploading(true);
      let { location } = await fileUpload(event.target.files[0]);
      setEditFormData({ ...Values, imageUrl: location });
      setCoverUploading(false);
    } else if (name === "galleryImages") {
      setImagesUploading(true);
      let images = [];
      let uploadedImages = [];
      images = event.target.files;
      console.log("images", images);
      for await (let image of images) {
        let { location } = await fileUpload(image);
        uploadedImages.push({ image: location });
        console.log("image uploaded");
      }
      if (uploadedImages.length > 5) {
        alert(
          "You can upload only 5 images , please select less than 5 Images!"
        );
        uploadedImages = [];
        setEditFormData({ ...Values, galleryImages: uploadedImages });
        setImagesUploading(false);
      } else {
        setEditFormData({ ...Values, galleryImages: uploadedImages });
        setImagesUploading(false);
      }
      uploadedImages = [];
    } else {
      let value = event.target.value;
      setEditFormData({ ...Values, [name]: value });
    }
  };

  // update service
  function EditformSubmit(serviceId) {
    setLoading(true);
    setmodalIsOpen(false);
    console.log("SERVICE ID", serviceId);

    for (var prop in EditFormData) {
      if (
        EditFormData[prop] === null ||
        EditFormData[prop] === undefined ||
        EditFormData[prop] == ""
      ) {
        delete EditFormData[prop];
      }
    }

    axios
      .put(`${API}api/admin/service/${serviceId}/edit`, EditFormData)
      .then((data) => {
        console.log("UPDATE SERVICE RESPONSE", data);
        setrefresh((data) => data + 1);
        setLoading(false);
      })
      .catch((err) => {
        console.log("ERROR", err);
        setLoading(false);
      });
  }

  // delete Service
  function deletemember(_id) {
    setLoading(true);
    console.log(_id);
    axios
      .delete(`${API}api/admin/service/${_id}/delete`)
      .then((data) => {
        console.log(data);
        setrefresh((data) => data + 1);
        setLoading(false);
      })
      .catch((err) => {
        console.log(err);

        setLoading(false);
      });
  }

  //* console logs
  console.log("Values", Values);
  console.log("Errors", Errors);

  //? changeHandler
  const changeHandler = (name) => async (event) => {
    if (name === "imageUrl") {
      setCoverUploading(true);
      let { location } = await fileUpload(event.target.files[0]);
      setValues({ ...Values, imageUrl: location });
      setCoverUploading(false);
    } else if (name === "galleryImages") {
      setImagesUploading(true);
      let images = [];
      let uploadedImages = [];
      images = event.target.files;
      console.log("images", images);
      for await (let image of images) {
        let { location } = await fileUpload(image);
        uploadedImages.push({ image: location });
        console.log("image uploaded");
      }
      if (uploadedImages.length > 5) {
        alert(
          "You can upload only 5 images , please select less than 5 Images!"
        );
        uploadedImages = [];
        setValues({ ...Values, galleryImages: uploadedImages });
        setImagesUploading(false);
      } else {
        setValues({ ...Values, galleryImages: uploadedImages });
        setImagesUploading(false);
      }
      uploadedImages = [];
    } else {
      let value = event.target.value;
      setValues({ ...Values, [name]: value });
    }
  };

  // handleChange
  const handleChange = (chips) => {};

  const handleAddChip = (chip) => {
    setChips([...Chips, chip]);
    setValues({ ...Values, keywords: [...Chips, chip] });
  };

  const handleDeleteChip = (chip, index) => {
    let remaningChips = Chips.filter((ch, indx) => indx != index);
    setChips(remaningChips);
  };

  // edit chips
  const editHandleChange = (chips) => {};

  const editHandleAddChip = (chip) => {
    setEditChips([...EditChips, chip]);
    // setEditFormData({ ...formData, Keywords: [...EditChips] });
  };
  const editHandleDeleteChip = (chip, index) => {
    let remainingChips = EditChips.filter((val, inx) => inx != index);
    setEditChips(remainingChips);
  };

  // conditional rendering
  if (Loading) {
    return <Loader />;
  }

  console.log("EDIT", edit);
  console.log("Values", Values);
  console.log("Image", Image);
  console.log("Values", Values);
  console.log("Images Uploading", ImagesUploading);
  console.log("EditFormData", EditFormData);
  console.log("imageRef", imageRef.current);
  console.log(
    "imageRef naturalWidth",
    imageRef.current && imageRef.current.naturalWidth
  );
  console.log(
    "imageRef naturalHeight",
    imageRef.current && imageRef.current.naturalHeight
  );

  // Component return
  return (
    <div className="container-fluid">
      <div className="burger">
        <div className="line1"></div>
        <div className="line2"></div>
        <div className="line3"></div>
      </div>
      <div className="row">
        <div class="col-md-2">
          {" "}
          <NavBar />
        </div>
        <div class="col-md-10 pt-5">
          <h1 className="text-primary">Service</h1>

          <Modalcomp title="Add a Service">
            <form onSubmit={submitHandler}>
              <TextField
                style={{ width: "100%" }}
                id="standard-basic"
                label="Service Name"
                defaultValue={name}
                onChange={changeHandler("name")}
                error={Errors.name}
                helperText={Errors.name}
              />
              <br />

              <TextField
                type="number"
                style={{ width: "100%" }}
                id="standard-basic"
                label="Amount"
                value={amount}
                onChange={changeHandler("amount")}
                error={Errors.amount}
                helperText={Errors.amount}
              />
              <br />
              <br />

              <div>
                <InputLabel id="demo-simple-select-label">
                  Cover Image
                </InputLabel>
                <Button variant="contained" component="label">
                  {CoverUploading ? "Uploading" : " Upload"}
                  <input
                    style={{ width: "100%" }}
                    onChange={changeHandler("imageUrl")}
                    type="file"
                    hidden
                  />
                </Button>
                {Errors.imageUrl && (
                  <FormHelperText style={{ color: "red" }}>
                    {Errors.imageUrl}
                  </FormHelperText>
                )}

                <div style={{ display: "none" }}>
                  <img
                    ref={imageRef}
                    src={imageUrl}
                    alt="image for validation"
                  />
                </div>
              </div>
              {/* <br /> */}
              {imageUrl ? (
                <>
                  Cover Image Selected
                  <br />
                  <br />
                </>
              ) : (
                ""
              )}

              <TextField
                type="number"
                style={{ width: "100%" }}
                id="standard-basic"
                label="Duration (in mins)"
                value={duration}
                onChange={changeHandler("duration")}
                error={Errors.duration}
                helperText={Errors.duration}
              />
              <br />
              <br />

              <InputLabel id="demo-simple-select-label">
                Gallery Images
              </InputLabel>
              <Button variant="contained" component="label">
                {ImagesUploading ? "Uploading Images" : "Upload"}
                <input
                  style={{ width: "100%" }}
                  onChange={changeHandler("galleryImages")}
                  type="file"
                  multiple="multiple"
                  hidden
                />
              </Button>
              <br />
              <br />

              {galleryImages && galleryImages.length > 0 ? (
                <>
                  {galleryImages.length} images selected for gallery images
                  <br />
                  <br />
                </>
              ) : (
                ""
              )}

              <div>
                <InputLabel id="demo-simple-select-label">Category</InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  onChange={changeHandler("category")}
                  value={category}
                >
                  {Categories &&
                    Categories.map((val) => {
                      return (
                        <option key={val._id} value={val._id}>
                          {val.categoryName}
                        </option>
                      );
                    })}
                </Select>
                {Errors.category && (
                  <FormHelperText style={{ color: "red" }}>
                    {Errors.category}
                  </FormHelperText>
                )}
              </div>
              <br />

              <InputLabel id="demo-simple-select-label">Status</InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={status}
              >
                <option value="true">True</option>
                <option value="false">False</option>
              </Select>
              <br />
              <br />

              <div>
                <InputLabel id="demo-simple-select-label">Partner</InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  onChange={changeHandler("partner")}
                  value={partner}
                >
                  {Partners &&
                    Partners.map((val) => {
                      return (
                        <option key={val._id} value={val._id}>
                          {val.name}
                        </option>
                      );
                    })}
                </Select>
                {Errors.partner && (
                  <FormHelperText style={{ color: "red" }}>
                    {Errors.partner}
                  </FormHelperText>
                )}
              </div>

              {/* <div>
                <InputLabel id="demo-simple-select-label">Keywords</InputLabel>
                <ChipInput
                  style={{ width: "100%" }}
                  value={Chips}
                  onChange={(chips) => handleChange(chips)}
                  onAdd={(chip) => handleAddChip(chip)}
                  onDelete={(chip, index) => handleDeleteChip(chip, index)}
                />

                {Errors.keywords && (
                  <FormHelperText style={{ color: "red" }}>
                    {Errors.keywords}
                  </FormHelperText>
                )}
              </div> */}
              <br />
              {/* <br /> */}
              <button className="btn btn-outline-primary">Add Service</button>
            </form>
          </Modalcomp>

          {edit && (
            <Modal
              isOpen={modalIsOpen}
              onRequestClose={closeModal}
              style={customStyles}
            >
              <br />
              <h2 style={{ color: "rgb(76, 68, 187)" }}>Edit</h2>
              <TextField
                style={{ width: "100%" }}
                label="ServiceName"
                onChange={editChangeHandler("name")}
                defaultValue={edit.name}
              />
              <br />
              <br />

              <TextField
                style={{ width: "100%" }}
                type="number"
                label="Amount"
                onChange={editChangeHandler("amount")}
                defaultValue={edit.amount}
              />
              <br />
              <br />

              <InputLabel id="demo-simple-select-label">Status</InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                onChange={editChangeHandler("status")}
                defaultValue={edit.status}
              >
                <MenuItem value="true">True</MenuItem>
                <MenuItem value="false">False</MenuItem>
              </Select>
              <br />
              <br />

              <InputLabel id="demo-simple-select-label">Cover Image</InputLabel>
              <Button variant="contained" component="label">
                {CoverUploading ? "Uploading" : " Upload"}
                <input
                  style={{ width: "100%" }}
                  onChange={editChangeHandler("imageUrl")}
                  type="file"
                  hidden
                />
              </Button>
              <br />
              <br />

              {EditFormData.imageUrl ? (
                <>
                  Cover Image Selected
                  <br />
                  <br />
                </>
              ) : (
                ""
              )}

              <TextField
                type="number"
                style={{ width: "100%" }}
                id="standard-basic"
                label="Duration (in mins)"
                defaultValue={edit.duration}
                onChange={editChangeHandler("duration")}
              />
              <br />
              <br />

              <InputLabel id="demo-simple-select-label">Partner</InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                onChange={editChangeHandler("partner")}
                defaultValue={edit.partner._id}
              >
                {Partners &&
                  Partners.map((val) => {
                    return (
                      <option key={val._id} value={val._id}>
                        {val.name}
                      </option>
                    );
                  })}
              </Select>
              <br />
              <br />

              <InputLabel id="demo-simple-select-label">
                Gallery Images
              </InputLabel>
              <Button variant="contained" component="label">
                Upload File
                <input
                  style={{ width: "100%" }}
                  onChange={editChangeHandler("galleryImages")}
                  type="file"
                  hidden
                />
              </Button>
              <br />
              <br />

              {EditFormData.galleryImages &&
              EditFormData.galleryImages.length > 0 ? (
                <>
                  {EditFormData.galleryImages.length} images selected for
                  gallery images
                  <br />
                  <br />
                </>
              ) : (
                ""
              )}

              <InputLabel id="demo-simple-select-label">Category</InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                onChange={(e) =>
                  setEditFormData({ ...EditFormData, category: e.target.value })
                }
                defaultValue={edit.category._id}
              >
                {Categories &&
                  Categories.map((val) => {
                    return (
                      <option key={val._id} value={val._id}>
                        {val.categoryName}
                      </option>
                    );
                  })}
              </Select>
              <br />
              <br />

              <Button
                color="primary"
                variant="outlined"
                onClick={() => EditformSubmit(edit._id)}
              >
                Submit
              </Button>
              <Button color="primary" variant="outlined" onClick={closeModal}>
                Close
              </Button>
            </Modal>
          )}

          <div className="mt-5 ">
            {Category && (
              <Tables
                Edit={(data) => {
                  setedit(data);
                  setmodalIsOpen(true);
                  setEditFormData({
                    ServiceName: data.ServiceName,
                    Status: data.Status,
                    amount: data.amount,
                    Keywords: data.Keywords,
                    _id: data._id,
                  });
                }}
                Delete={(_id) => {
                  deletemember(_id);
                }}
                rows={Category}
                columns={[
                  "No",
                  "Service Name",
                  "Amount",
                  "Duration",
                  "Partner Name",
                  "Category Name",
                  "Status",
                  "Actions",
                ]}
              />
            )}
          </div>
        </div>
      </div>
    </div>
  );
}

export default Services;
