import React from "react";

const FileInput = ({ changeHandler, Values, width }) => {
  let { photo } = Values;

  return (
    <div
      style={{
        position: "relative",
        width: width ? width : "100%",
        display: "flex",
        alignItems: "center",
        border: "1px solid lightgrey",
        height: "fit-content",
        borderRadius: "5px",
      }}
    >
      <input
        // type="file"
        style={{
          position: "relative",
          zIndex: "2",
          width: "100%",
          height: "calc(1.5em + .75rem + 2px)",
          margin: "0",
          opacity: "0",
          border: "1px dotted black",
        }}
      />
      <button
        style={{
          height: "100%",
          padding: "5px",
          outline: "none",
          border: "none",
        }}
      >
        <div style={{ position: "relative" }}>
          <input
            onChange={changeHandler("photo")}
            style={{
              width: "70px",
              position: "relative",
              zIndex: "2",
              height: "calc(1.5em + .75rem + 2px)",
              margin: "0",
              opacity: "0",
              cursor: "pointer",
            }}
            type="file"
          />
          <p
            style={{
              position: "absolute",
              top: "7px",
              right: "7px",
            }}
          >
            Browse
          </p>
        </div>
      </button>
      <label style={{ position: "absolute", left: "10px" }}>
        {photo ? `${photo.name.substring(0, 25)}...` : ""}
      </label>
    </div>
  );
};

export default FileInput;
