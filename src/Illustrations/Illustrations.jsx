import { FormHelperText, TextField } from "@material-ui/core";
import toast, { Toaster } from "react-hot-toast";

//? Components
import Navbar from "../Navbar/Navbar";
import Modalcomp from "../Modal/Modal";
import { useEffect, useRef, useState } from "react";
import Table from "../Tables/IllustrationTable";
import FileInput from "./FileInput/FileInput";
import { fileUpload } from "../uploads/UploadFile";
import axios from "axios";
import Loader from "../loader/Loader";
import Modal from "./Modal/Modal";

//? Material-ui
import { Button } from "@material-ui/core";

//? Backend
import { API } from "../Backend";

const Illustration = () => {
  let imageRef = useRef(null);

  const [Data, setData] = useState("");
  const [open, setOpen] = useState(false);
  const [ImageUploading, setImageUploading] = useState(false);
  const [Refresh, setRefresh] = useState(1);
  const [Loading, setLoading] = useState(true);
  const [search, setSearch] = useState("");
  const [Illustrations, setIllustrations] = useState([]);
  const [Values, setValues] = useState({
    pageName: "",
    link: "",
    photo: "",
  });
  const [FormError, setFormError] = useState({
    pageName: false,
    photo: false,
    link: false,
  });

  //? Destructuring
  let { pageName, link, photo } = Values;

  //? useEffect
  useEffect(() => {
    axios
      .get(`${API}api/get/illustrations`)
      .then((response) => {
        console.log("get illustrations response data", response);
        setIllustrations(response.data);
        setLoading(false);
      })
      .catch((err) => {
        console.log({ err });
        setLoading(false);
      });
  }, [Refresh]);

  //? changeHandler
  const changeHandler = (name) => async (e) => {
    console.log({ name });
    if (name == "photo") {
      setImageUploading(true);
      console.log("pic", e.target.files[0]);
      try {
        let { location } = await fileUpload(e.target.files[0]);
        setValues({ ...Values, link: location, photo: e.target.files[0] });
        setImageUploading(false);

        return;
      } catch (err) {
        console.log({ err });
        setImageUploading(false);

        return;
      }
    }

    let value = e.target.value;
    setValues({ ...Values, [name]: value });
  };

  //? submitHandler
  const submitHandler = (e) => {
    e.preventDefault();

    if (Validate()) {
      setLoading(true);

      axios
        .post(`${API}api/add/illustration`, Values)
        .then((res) => {
          console.log({ res });
          toast.success(res.data.message);
          setValues({
            link: "",
            photo: "",
            pageName: "",
          });
          setRefresh(Refresh + 1);
        })
        .catch((error) => {
          console.log("error", error);
          setLoading(false);
        });
    }
  };

  //? deleteHandler
  const deleteHandler = (id) => {
    setLoading(true);
    axios
      .delete(`${API}api/illustration/${id}/delete`)
      .then((response) => {
        console.log("Delete Handler Response", response);
        toast.success(response.data.message);

        setRefresh(Refresh + 1);
      })
      .catch((err) => {
        console.log("err in delete handler", err);
        setLoading(false);
      });
  };

  //? Validate
  const Validate = () => {
    let value = true;

    let err = {
      pageName: false,
      photo: false,
      link: false,
    };

    if (pageName == "") {
      err.pageName = "Enter a Page Name First!";
      value = false;
    }

    if (photo == "") {
      err.photo = "Please Select a Image First!";
      value = false;
    }

    //? validation for image size
    if (photo.size && photo.size / 1000 > 1000) {
      err.photo = "Please Select a Image under the size of 1 mb!";
      value = false;
    }

    let { current } = imageRef;

    //? validation for image width
    // if (current && current.naturalWidth && current.naturalWidth != 1680) {
    //   err.photo = "Please Select a Image with a width of 1680px!";
    //   value = false;
    // }

    // //? validation for image height
    // if (current && current.naturalHeight && current.naturalHeight != 1050) {
    //   err.photo = "Please Select a Image with a Height of 1050px!";
    //   value = false;
    // }

    if (link == "") {
      err.link = "Please Select a Image First!";
      value = false;
    }

    setFormError({ ...err });
    return value;
  };

  console.log({ Values });
  console.log({ FormError });
  console.log({ Illustrations });
  console.log({ search });

  if (Loading) {
    return <Loader />;
  }

  let { current } = imageRef;
  console.log("current", imageRef.current);
  console.log(
    "current naturalWidth",
    current && current.naturalWidth && current.naturalWidth
  );
  console.log(
    "current naturalHeight",
    current && current.naturalHeight && current.naturalHeight
  );

  return (
    <div className="container-fluid">
      <div className="burger">
        <div className="line1"></div>
        <div className="line2"></div>
        <div className="line3"></div>
      </div>
      <div className="row">
        <div className="col-md-2">
          <Navbar />
        </div>
        <div className="col-md-10 pt-5">
          <Toaster />
          <h1 className="text-primary">Illustrations</h1>
          <h4 className="mt-3 mb-3 text-muted">Search</h4>

          {
            <Modal open={open} setOpen={setOpen}>
              <div style={{ width: "500px", height: "500px" }}>
                <img
                  style={{ width: "100%", height: "100%", objectFit: "cover" }}
                  src={Data}
                  alt="illustration-image"
                />
              </div>
            </Modal>
          }

          <TextField
            size="small"
            variant="outlined"
            label="Enter name or email id..."
            onChange={(event) => {
              setSearch(event.target.value);
            }}
          ></TextField>

          <Modalcomp title="Add Illustration">
            <TextField
              style={{ width: "100%" }}
              value={Values.pageName}
              onChange={changeHandler("pageName")}
              error={FormError.pageName}
              helperText={FormError.pageName}
              id="outlined-error-helper-text"
              label="Page Name"
              variant="outlined"
            />
            <br />
            <br />
            <FileInput Values={Values} changeHandler={changeHandler} />
            {
              <FormHelperText error={FormError.link || FormError.photo}>
                {FormError.link || FormError.photo}
              </FormHelperText>
            }
            <br />
            <Button onClick={submitHandler} color="primary" variant="contained">
              {ImageUploading ? "Uploading" : "Add Illustration"}
            </Button>
            <div style={{ display: "none" }}>
              <img ref={imageRef} src={link} alt="img for validation" />
            </div>
          </Modalcomp>

          <div className="mt-5">
            <Table
              Delete={(data) => {
                console.log("Data in Delete ", data);
                deleteHandler(data._id);
              }}
              Show={(data) => {
                setData(data.link);
                setOpen(true);
              }}
              rows={Illustrations ? Illustrations : []}
              search={search}
              columns={["Page Name", "Image", "Action"]}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Illustration;
