import React from "react";
import {
  Modal,
  Backdrop,
  Fade,
  makeStyles,
  IconButton,
} from "@material-ui/core";
import Close from "@material-ui/icons/Close";

const useStyles = makeStyles((theme) => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    // position: "static",
    width: "100%",
    margin: "auto",
    height: "100%",
    overflow: "scroll",
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2),
    borderRadius: "10px",
    width: "auto",
    minWidth: "400px",
    height: "auto",
    position: "relative",
    margin: "20px",
  },
}));

const TransitionModal = ({ open, setOpen, children }) => {
  const classes = useStyles();

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <>
            <div className={classes.paper}>{children}</div>
          </>
        </Fade>
      </Modal>
    </div>
  );
};

export default TransitionModal;
