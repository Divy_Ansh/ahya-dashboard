import React, { useEffect, useState, useContext } from "react";
import { useForm } from "react-hook-form";
import Modalcomp from "../Modal/Modal";
import axios from "axios";
import Tables from "../Tables/PartnerTicketsTable";
import NavBar from "../Navbar/Navbar";
import Modal from "react-modal";
import { TextField } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import { NavbarContext } from "../ContextApi/NavbarProvider";

import { InputLabel } from "@material-ui/core";
import { API } from "../Backend";
import { addMember } from "../helpers/helper";

import Loader from "../loader/Loader";

const PartnerTickets = () => {
  const [Tickets, setTickets] = useState([]);
  const [Loading, setLoading] = useState(true);
  const [Reload, setReload] = useState(false);

  const [modalIsOpen, setmodalIsOpen] = useState(false);
  const [EditTicket, setEditTicket] = useState({
    title: "",
    description: "",
    subject: "",
    ticketId: "",
    reply: "",
  });

  const [FormError, setFormError] = useState({
    reply: false,
  });

  const { reply, ticketId } = EditTicket;
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const [refresh, setrefresh] = useState(1);

  function openModal() {
    setmodalIsOpen(true);
  }

  function closeModal() {
    setmodalIsOpen(false);
  }

  const customStyles = {
    content: {
      top: "50%",
      left: "50%",
      right: "auto",
      bottom: "auto",
      width: "30%",
      marginRight: "-50%",
      transform: "translate(-50%, -50%)",
    },
  };

  const getTickets = () => {
    axios
      .get(`${API}api/admin/get/partner/tickets`)
      .then((data) => {
        console.log("PARTNER TICKETS", data.data);

        if (data.error) {
          alert(data.message);
        }
        setTickets(data.data);
        setLoading(false);
      })
      .catch((err) => {
        console.log(err);
        setLoading(false);
      });
  };

  // useEffect
  useEffect(() => {
    getTickets();
  }, [Reload]);

  // Conditional rendering
  if (Loading) {
    return <Loader />;
  }

  const replyHandler = (ticketId) => {
    if (Validate()) {
      setLoading(true);

      axios
        .put(`${API}api/admin/ticket/${ticketId}/reply`, EditTicket, {
          headers: {
            "Content-Type": "application/json",
          },
        })
        .then((data) => {
          console.log("TICKET STATUS RESPONE", data);
          setmodalIsOpen(false);
          setReload(!Reload);
          setLoading(false);
        })
        .catch((err) => {
          console.error("ERROR", err);
          setmodalIsOpen(false);
          setLoading(false);
        });
    }
  };

  //? validation
  const Validate = () => {
    let value = true;

    let err = {
      reply: false,
    };

    if (reply == "" || reply == null) {
      value = false;
      err.reply = "Please Enter Reply First!";
    }

    setFormError({ ...err });
    return value;
  };

  console.log("FormError", FormError);

  return (
    <>
      <div className="container-fluid">
        <div className="burger">
          <div className="line1"></div>
          <div className="line2"></div>
          <div className="line3"></div>
        </div>
        <div className="row">
          <div className=" col-md-2">
            {" "}
            <NavBar />
          </div>
          <div className="col-md-10 pt-5">
            <h1 className="text-primary">Partner-Tickets</h1>
            {/*  */}
            {EditTicket && (
              <Modal
                isOpen={modalIsOpen}
                onRequestClose={closeModal}
                style={customStyles}
              >
                <h3 style={{ textAlign: "center" }}>
                  QueryType - {EditTicket.subject}
                </h3>
                <h6>Title - {EditTicket.title}</h6>

                <h6>Descrition - {EditTicket.description}</h6>
                <br />
                <TextField
                  style={{ width: "100%" }}
                  id="outlined-multiline-static"
                  label="Reply"
                  multiline
                  rows={4}
                  defaultValue=""
                  onChange={(e) =>
                    setEditTicket({ ...EditTicket, reply: e.target.value })
                  }
                  variant="outlined"
                  error={FormError.reply}
                  helperText={FormError.reply}
                />
                <Button
                  onClick={() => replyHandler(ticketId)}
                  style={{ marginTop: "10px" }}
                  variant="contained"
                  color="primary"
                >
                  Send Reply
                </Button>
              </Modal>
            )}
            {/*  */}
            <div className="mt-5 ">
              <Tables
                Edit={(data) => {
                  console.log("EDIT TICKET", data);
                  setEditTicket({
                    ticketId: data._id,
                    title: data.queryType,
                    description: data.description,
                    subject: data.subject,
                  });

                  setmodalIsOpen(true);
                }}
                rows={Tickets}
                columns={[
                  "Partner",
                  "Subject",
                  "Query-Type",
                  "Description",
                  "Mobile",
                  "Action",
                ]}
              />
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default PartnerTickets;
