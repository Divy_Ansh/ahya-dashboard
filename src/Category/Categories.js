import React, { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import Modalcomp from "../Modal/Modal";
import axios from "axios";
import Tables from "../Tables/CategoryTable";
import NavBar from "../Navbar/Navbar";
import Modal from "react-modal";
import { TextField, InputLabel } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Select from "@material-ui/core/Select";
import { MenuItem, FormHelperText } from "@material-ui/core";
import ChipInput from "material-ui-chip-input";

import { API } from "../Backend";
import Loader from "../loader/Loader";
import { fileUpload } from "../uploads/UploadFile";

function Categories() {
  const [edit, setedit] = useState("");
  let [category, setcategory] = useState([]);

  const [ImageUploading, setImageUploading] = useState(false);
  const [refresh, setrefresh] = useState(1);
  const [Loading, setLoading] = useState(false);
  const [modalIsOpen, setmodalIsOpen] = useState(false);
  const [FormError, setFormError] = useState({
    categoryName: false,
    imageUrl: false,
  });
  const [UpdateFormError, setUpdateFormError] = useState({
    categoryName: false,
  });
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  function openModal() {
    setmodalIsOpen(true);
  }
  function closeModal() {
    setmodalIsOpen(false);
  }
  const customStyles = {
    content: {
      top: "50%",
      left: "50%",
      right: "auto",
      bottom: "auto",
      width: "25%",
      marginRight: "-50%",
      transform: "translate(-50%, -50%)",
    },
  };
  const [Chips, setChips] = useState([]);
  const [EditChips, setEditChips] = useState([]);
  const [formData, setFormData] = React.useState({
    categoryName: "",
    imageUrl: "",
    status: "",
    Keywords: [],
    uploadImage: false,
  });

  const [Values, setValues] = React.useState({
    categoryName: "",
    imageUrl: "",
    status: "true",
    keywords: [],
  });

  // destructuring
  const { categoryName, imageUrl, status, keywords } = Values;

  // useEffect hooks
  useEffect(() => {
    Getcategory();
  }, [refresh]);

  // getAllCategories
  const Getcategory = () => {
    setLoading(true);

    axios
      .get(`${API}api/admin/all/categories`)
      .then((data) => {
        setcategory(data.data);
        console.log("ALL CATEGORIES", data.data);
        setLoading(false);
      })
      .catch((err) => {
        console.log(err);
        setLoading(false);
      });
  };

  // submitHandler for adding new categories
  const submitHandler = (e) => {
    e.preventDefault();
    if (Validate()) {
      setLoading(true);

      axios
        .post(`${API}api/admin/category/create`, {
          categoryName,
          status,
          imageUrl,
          keywords: [...Chips],
        })
        .then((data) => {
          console.log("NEW CATEGORY DATA", data);
          setrefresh((data) => data + 1);
          setLoading(false);
          setValues({
            categoryName: "",
            imageUrl: "",
            status: "true",
            keywords: [],
          });
        })
        .catch((err) => {
          console.log(err);
          setLoading(false);
        });
    }
  };

  // editHandler
  function EditformSubmit(categoryId) {
    if (updateValidate()) {
      setFormData({ ...formData, Keywords: EditChips });

      console.log(formData);
      axios
        .put(`${API}api/admin/category/${categoryId}/edit`, formData)
        .then((data) => {
          console.log(data);
          setrefresh((data) => data + 1);
          setFormData({
            categoryName: "",
            imageUrl: "",
            status: "",
            Keywords: [],
            uploadImage: false,
          });
        })
        .catch((err) => {
          console.log(err);
        });
      closeModal();
    }
  }

  // deleteMember
  function deletemember(_id) {
    console.log(_id);
    axios
      .delete(`${API}api/admin/category/${_id}/delete`)
      .then((data) => {
        console.log(data);
        setrefresh((data) => data + 1);
      })
      .catch((err) => {
        console.log(err);
      });
  }

  // changeHandler
  const changeHandler = (name) => async (e) => {
    if (name == "imageUrl") {
      setImageUploading(true);
      let { location } = await fileUpload(e.target.files[0]);
      setValues({ ...Values, imageUrl: location });
      setImageUploading(false);
      return;
    }
    let value = e.target.value;
    setValues({ ...Values, [name]: value });
  };

  const editChangeHandler = (name) => async (e) => {
    if (name == "imageUrl") {
      setImageUploading(true);
      let { location } = await fileUpload(e.target.files[0]);
      setFormData({ ...formData, imageUrl: location, uploadImage: true });
      setImageUploading(false);
      return;
    }
    let value = e.target.value;
    setFormData({ ...formData, [name]: value });
  };

  // handleChange
  const handleChange = (chips) => {};

  const handleAddChip = (chip) => {
    setChips([...Chips, chip]);
  };

  console.log("CHIPS", Chips);
  console.log("EDIT CHIPS", EditChips);

  const handleDeleteChip = (chip, i) => {
    let remainingKeywords = Chips.filter((word, ind) => ind != i);
    setChips(remainingKeywords);
  };

  // for editing existing chips
  const editHandleChange = (chips) => {};

  const editHandleAddChip = (chip) => {
    setEditChips([...EditChips, chip]);
    setFormData({ ...formData, Keywords: [...EditChips] });
  };
  const editHandleDeleteChip = (chip, index) => {
    let remainingChips = EditChips.filter((val, inx) => inx != index);
    setEditChips(remainingChips);
  };

  useEffect(() => {
    if (edit) {
      setEditChips(edit.keywords);
    }
  }, [edit]);

  // conditional rendering
  if (Loading) {
    return <Loader />;
  }

  //? Validation
  const Validate = () => {
    let value = true;

    let err = {
      categoryName: false,
      imageUrl: false,
    };

    if (categoryName == "") {
      value = false;
      err.categoryName = "Please Enter Name First!";
    }

    if (imageUrl == "") {
      value = false;
      err.imageUrl = "Please Select a Image First!";
    }

    setFormError({ ...err });
    return value;
  };

  const updateValidate = () => {
    let value = true;

    let err = {
      categoryName: false,
    };

    if (formData.categoryName == "") {
      value = false;
      err.categoryName = "Category name Can't be Empty!";
    }

    setUpdateFormError({ ...err });
    return value;
  };

  console.log("Edit", edit);
  console.log("formData", formData);

  console.log("Values", Values);

  // Categories return
  return (
    <div className="container-fluid">
      <div className="burger">
        <div className="line1"></div>
        <div className="line2"></div>
        <div className="line3"></div>
      </div>
      <div className="row">
        <div class="col-md-2">
          {" "}
          <NavBar />
        </div>
        <div class="col-md-10  pt-5">
          <h1 className="text-primary">Category</h1>

          <Modalcomp title="Add a Category">
            <form onSubmit={submitHandler}>
              <TextField
                style={{ width: "100%" }}
                id="standard-basic"
                label="Category Name"
                value={categoryName}
                onChange={changeHandler("categoryName")}
                error={FormError.categoryName}
                helperText={FormError.categoryName}
              />
              <br />
              <br />

              <Button variant="contained" component="label">
                {ImageUploading ? "Uploading" : " Upload"}
                <input
                  style={{ width: "100%" }}
                  onChange={changeHandler("imageUrl")}
                  type="file"
                  hidden
                />
              </Button>
              <br />
              <br />

              {FormError.imageUrl && (
                <>
                  <FormHelperText style={{ color: "red" }}>
                    {FormError.imageUrl}
                  </FormHelperText>
                  <br />
                </>
              )}

              {imageUrl ? (
                <>
                  Image Selected
                  <br />
                  <br />
                </>
              ) : (
                ""
              )}

              <InputLabel id="demo-simple-select-label">Status</InputLabel>
              <Select onChange={changeHandler("status")} value={status}>
                <MenuItem value="true">True</MenuItem>
                <MenuItem value="false">False</MenuItem>
              </Select>
              <br />
              <br />
              {/* 
              <InputLabel id="demo-simple-select-label">Keywords</InputLabel>
              <ChipInput
                style={{ width: "100%" }}
                value={Chips}
                onChange={(chips) => handleChange(chips)}
                onAdd={(chip) => handleAddChip(chip)}
                onDelete={(chip, index) => handleDeleteChip(chip, index)}
              /> */}
              <Button
                color="primary"
                size="medium"
                className="bg-primary text-white"
                type="submit"
              >
                Add
              </Button>
            </form>
          </Modalcomp>

          {edit && (
            <Modal
              isOpen={modalIsOpen}
              onRequestClose={closeModal}
              style={customStyles}
            >
              <br />
              <h2 style={{ color: "rgb(76, 68, 187)" }}>Edit</h2>
              <TextField
                label="Category Name"
                style={{ width: "100%" }}
                onChange={editChangeHandler("categoryName")}
                value={formData.categoryName}
                error={UpdateFormError.categoryName}
                helperText={UpdateFormError.categoryName}
              ></TextField>
              <br />
              <br />

              <Button variant="contained" component="label">
                {ImageUploading ? "Uploading" : " Upload"}
                <input
                  style={{ width: "100%" }}
                  onChange={editChangeHandler("imageUrl")}
                  type="file"
                  hidden
                />
              </Button>
              <br />
              <br />
              {formData.uploadImage ? (
                <>
                  Image Selected
                  <br />
                  <br />
                </>
              ) : (
                ""
              )}

              <InputLabel id="demo-simple-select-label">Status</InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                onChange={editChangeHandler("status")}
                value={formData.status}
              >
                <MenuItem value="true">True</MenuItem>
                <MenuItem value="false">False</MenuItem>
              </Select>
              <br />
              <br />

              {/* <TextField
                label="Image URL"
                style={{ width: "100%" }}
                onChange={(e) =>
                  setFormData({ ...formData, imageUrl: e.target.value })
                }
                defaultValue={edit.imageUrl}
              ></TextField>{" "} */}
              <br />
              <br />
              {/* <InputLabel id="demo-simple-select-label">Keywords</InputLabel>
              <ChipInput
                style={{ width: "100%" }}
                defaultValue={edit.keywords}
                value={EditChips}
                onChange={(chips) => editHandleChange(chips)}
                onAdd={(chip) => editHandleAddChip(chip)}
                onDelete={(chip, index) => editHandleDeleteChip(chip, index)}
              /> */}
              <Button
                color="primary"
                variant="outlined"
                onClick={() => EditformSubmit(edit._id)}
              >
                Submit
              </Button>
              <Button color="primary" variant="outlined" onClick={closeModal}>
                Close
              </Button>
            </Modal>
          )}

          <div className="mt-5 ">
            {category && (
              <Tables
                Edit={(data) => {
                  setedit(data);
                  setmodalIsOpen(true);
                  setFormData({
                    categoryName: data.categoryName,
                    status: data.status,
                    imageUrl: data.imageUrl,
                    _id: data._id,
                  });
                }}
                Delete={(_id) => {
                  deletemember(_id);
                  console.log(_id);
                }}
                rows={category}
                columns={[
                  "No",
                  "Category Name",
                  "Image URL",
                  "Status",
                  "Actions",
                ]}
              />
            )}
          </div>
        </div>
      </div>
    </div>
  );
}

export default Categories;
